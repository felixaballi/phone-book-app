package com.musalasoft.tests;

import org.junit.Test;

import java.util.regex.Pattern;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/19/2018.
 */

public class PhoneTest {
    private static final String BULGARIAN_PHONE_PATTERN = "^((0|\\+359)|(00)359)(87|88|89)([2-9])([0-9]{6})$";

    @Test
    public void validPhoneFormat() {
        String plus359Prefix = "+359899010101";
        String onlyOneZero = "0899010101";
        String zeroZeroPrefix = "00359899010101";

        assert Pattern.matches(BULGARIAN_PHONE_PATTERN, plus359Prefix);
        assert Pattern.matches(BULGARIAN_PHONE_PATTERN, onlyOneZero);
        assert Pattern.matches(BULGARIAN_PHONE_PATTERN, zeroZeroPrefix);
    }

    @Test
    public void invalidPhoneFormat() {
        String plus359PrefixButHigherProvider = "+359909010101";
        String onlyOneZeroWithExtraDigits = "08990101011";
        String zeroZeroPlusSign359Prefix = "+00359899010101";

        assert !Pattern.matches(BULGARIAN_PHONE_PATTERN, plus359PrefixButHigherProvider);
        assert !Pattern.matches(BULGARIAN_PHONE_PATTERN, onlyOneZeroWithExtraDigits);
        assert !Pattern.matches(BULGARIAN_PHONE_PATTERN, zeroZeroPlusSign359Prefix);
    }
}
