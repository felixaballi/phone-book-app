package com.musalasoft.core;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/19/2018.
 */
public class PhoneFactory {

    public static Phone create(String pName, String pNumber){
        return new Phone(pName, pNumber);
    }
}
