package com.musalasoft.core;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/18/2018.
 */
public class PhoneLogger {

    private final static Map<String, Integer> outgoingCalls = new LinkedHashMap<>();

    public static void addCall(Phone phone) {
        incrOutgoing(phone);
    }

    public static void exportAll(String filepath, PhoneBook book) throws IOException {

        if (!book.isEmpty()) {

            StringBuilder output = new StringBuilder();
            try (Writer writer = new FileWriter(filepath)) {
                System.out.println(String.format("\n(Exported file) => %s", filepath));
                System.out.println("==============================================================");
                writer.write(book.toString());
                System.out.print(book);
            }
        }
    }

    public static void importToBook(String filepath, PhoneBook book) {
        File file = Paths.get(filepath).toFile();
        if ((!Objects.isNull(book)) && (file.exists() && file.isFile())) {
            try (Scanner scanner = new Scanner(filepath)) {
                while (scanner.hasNext()) {
                    String[] parts = scanner.next().split(",");
                    if (parts.length == 2) {
                        Phone phn = new Phone(parts[0], parts[1]);
                        if (PhoneRepository.validatePhone(phn)) {
                            book.add(phn);
                        }
                    }
                }
            }
        }
    }

    public static PhoneBook importAll(String filepath) throws IOException {
        File file = Paths.get(filepath).toFile();
        if (file.exists() && file.isFile()) {
            try (Scanner scanner = new Scanner(Paths.get(filepath))) {
                while (scanner.hasNext()) {
                    String[] parts = scanner.next().split(",");
                    if (parts.length == 2) {
                        Phone phn = new Phone(parts[0], parts[1]);
                        PhoneRepository.addContact(phn);
                    }
                }
            }
        }

        System.out.println("\n(Imported) => Phone Book");
        System.out.println("==============================================================");
        PhoneBook phoneBook = PhoneRepository.getPhoneBook();
        System.out.print(phoneBook);
        return phoneBook;
    }

    private static void incrOutgoing(String pNumber) {
        if (outgoingCalls.containsKey(pNumber)) {
            Integer outgoingCounter = outgoingCalls.get(pNumber);
            outgoingCalls.put(pNumber, ++outgoingCounter);
        } else {
            outgoingCalls.putIfAbsent(pNumber, 1);
        }
    }

    private static void incrOutgoing(Phone phone) {
        if (outgoingCalls.containsKey(phone.getNumber())) {
            Integer outgoingCounter = outgoingCalls.get(phone.getNumber());
            outgoingCalls.put(phone.getNumber(), ++outgoingCounter);
        } else {
            outgoingCalls.putIfAbsent(phone.getNumber(), 1);
        }
    }

    public static Map<String, Integer> getOutgoingCalls() {
        return outgoingCalls;
    }

    public static void printSortedPhones() {
        List<Phone> contacts = PhoneRepository.getPhoneBook().getContacts();
        contacts.sort(Comparator.comparing(Phone::getName));
        System.out.println("\n(Sorted Phones) => By Name");
        System.out.println("==============================================================");
        contacts.forEach(System.out::println);
    }

    public static void printLargestFiveWithOutgoingCalls() {
        List<PhoneCall> calls = PhoneLogger.getOutgoingCalls()
                .entrySet().stream()
                .map(f -> new PhoneCall(f.getKey(), f.getValue()))
                .collect(Collectors.toList());

        calls.sort(new PhoneCallComparator());/* DESC */
        System.out.println("\n(First 5) => Outgoing Calls");
        System.out.println("==============================================================");
        for (int i = 0; i < 5; i++) {
            PhoneCall phoneCall = calls.get(i);
            System.out.println(phoneCall.getNumber() + ", " + phoneCall.getTimes());
        }
    }
}
