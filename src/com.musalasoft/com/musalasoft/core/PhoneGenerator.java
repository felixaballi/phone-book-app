package com.musalasoft.core;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.*;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/19/2018.
 */
public class PhoneGenerator {

    private final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    private final static Random rnd = new Random();
    private final static List<Phone> contacts = new ArrayList<>();

    public static void generateFile(String filepath, Integer times) throws IOException {
        try (Writer writer = new FileWriter(filepath)) {
            while (times > 0) {
                Phone phnGen = PhoneFactory.create(nameGen(), numberGen());
                PhoneRepository.addContact(phnGen);
                times--;
            }

            System.out.println(String.format("\n(Generated file) => %s", filepath));
            System.out.println("==============================================================");
            writer.write(String.valueOf(PhoneRepository.getPhoneBook()));
            System.out.print(PhoneRepository.getPhoneBook());
        }
    }

    private static String nameGen() throws IOException {
        return stringGen(20);
    }

    private static String numberGen() {
        String[] prefixes = {"+359", "00359", "0"};
        String[] providers = {"87", "88", "89"};

        StringBuilder output = new StringBuilder();
        String nextPrefix = prefixes[Math.abs(rnd.nextInt(prefixes.length))];
        output.append(nextPrefix);

        String nextProvider = providers[Math.abs(rnd.nextInt(providers.length))];
        output.append(nextProvider);

        int next = Math.abs(rnd.nextInt(9));
        int nextDigit = next == 0 ? ++next : next;
        output.append(nextDigit);

        for (int i = 0; i < 6; i++) {
            int nextD = Math.abs(rnd.nextInt(9));
            output.append(nextD);
        }

        return output.toString();
    }

    private static String stringGen(Integer length) throws IOException {
        char[] characters = ALPHABET.toCharArray();
        StringBuilder output = new StringBuilder(length);
        while (length > 0) {
            int index = Math.abs(rnd.nextInt(characters.length));
            output.append(characters[index <= 0 ? 1 : index]);
            length--;
        }

        return output.toString();
    }

    public static Map<String, Integer> generateCalls(Integer times) {
        PhoneBook phoneBook = PhoneRepository.getPhoneBook();
        while (times > 0) {
            Phone phone = phoneBook.getContacts().get(rnd.nextInt(phoneBook.getContacts().size()));
            PhoneLogger.addCall(phone);
            times--;
        }

        System.out.println("\n(Generated Calls) => Phone Book");
        System.out.println("==============================================================");
        StringBuilder output = new StringBuilder();
        for (Map.Entry<String, Integer> entry : PhoneLogger.getOutgoingCalls().entrySet()) {
            output.append(entry.getKey())
                    .append(",")
                    .append(entry.getValue())
                    .append("\n");
        }
        System.out.print(output);
        return PhoneLogger.getOutgoingCalls();
    }
}
