package com.musalasoft.core;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/18/2018.
 */
public class Phone {

    private String name;
    private String number;

    public Phone(String pName, String pNumber) {
        name = pName;
        number = pNumber;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public void setName(String pName) {
        name = pName;
    }

    public void setNumber(String pNumber) {
        number = pNumber;
    }

    @Override
    public String toString() {
        return String.format("%s,%s", getName(), getNumber());
    }
}
