package com.musalasoft.core;

import java.util.Comparator;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/18/2018.
 */
class PhoneCallComparator implements Comparator<PhoneCall> {

    @Override
    public int compare(PhoneCall o1, PhoneCall o2) {
        return o2.getTimes() - o1.getTimes();
    }
}
