package com.musalasoft.core;

import java.io.*;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/18/2018.
 */
public class Screen {

    public static void main(String[] args) throws IOException {
        System.out.println("Phone Book Application");
        System.out.println("==============================================================\n");

        while (System.in.read() == '\n' || System.in.read() == '\r') {
            System.out.println("Enter file path: \n");
        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String filepath = "" + reader.readLine() + "";

            /* String filepath = "E:\\phone_book.txt"; */
            PhoneGenerator.generateFile(filepath, 20);
            PhoneLogger.printSortedPhones();

            PhoneGenerator.generateCalls(40);
            PhoneLogger.printLargestFiveWithOutgoingCalls();

            Phone firstPhone = PhoneRepository.getPhoneBook().getContacts().get(0);
            PhoneRepository.remove(firstPhone.getName());
            /* After deletion */
            PhoneLogger.printSortedPhones();
//        PhoneBook phoneBook = PhoneLogger.importAll(filepath);
        }
    }
}
