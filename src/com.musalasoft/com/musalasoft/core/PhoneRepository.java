package com.musalasoft.core;

import java.util.*;
import java.io.FileNotFoundException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/18/2018.
 */
public class PhoneRepository {

    private final static PhoneBook book = PhoneBook.getInstance();
    private static final String BULGARIAN_PHONE_PATTERN = "^((0|\\+359)|(00)359)(87|88|89)([2-9])([0-9]{6})$";

    public final static PhoneBook getPhoneBook() {
        return book;
    }

    public final static boolean validatePhone(Phone phone) {
        /* Business rules here */
        return Pattern.matches(BULGARIAN_PHONE_PATTERN, phone.getNumber());
    }


    public static void addContact(Phone phone) {
        if (validatePhone(phone)) {
            book.add(phone);
        }
    }

    public static void addContact(String pName, String pNumber) {
        Phone phone = new Phone(pName, pNumber);
        addContact(phone);
    }

    public static void remove(Phone phone) {
        book.remove(phone);
    }

    public static void remove(String pName) {
        if (pName.length() > 0) {
            getPhoneByName(pName).ifPresent(book::remove);
        }
    }

    public static Optional<Phone> getPhoneByName(String pName) {
        Optional<Phone> found = book.getContacts()
                .stream()
                .filter(p -> p.getName().equals(pName))
                .collect(Collectors.toList())
                .stream()
                .findFirst();

        return found;
    }
}
