package com.musalasoft.core;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/18/2018.
 */
public class PhoneBook {

    private final static List<Phone> contacts = new ArrayList<Phone>();

    private PhoneBook() {
    }

    public final static PhoneBook getInstance() {
        return new PhoneBook();
    }

    public List<Phone> getContacts() {
        return contacts;
    }

    public void add(Phone phone) {
        contacts.add(phone);
    }

    public void remove(Phone phone) {
        contacts.remove(phone);
    }

    public boolean isEmpty() {
        return contacts.size() == 0;
    }

    @Override
    public String toString() {
        if (contacts.isEmpty()) return "\nEmpty contacts list\n";
        StringBuilder output = new StringBuilder(contacts.size());
        for (Phone phn : contacts) {
            output.append(phn).append("\n");
        }

        return output.toString();
    }
}
