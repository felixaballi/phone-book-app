package com.musalasoft.core;

/**
 * Created by Felix Aballi (felixaballi@gmail.com) on 4/19/2018.
 */
public class PhoneCall {
    private String number;
    private Integer times;

    public PhoneCall(String key, Integer value) {
        number = key;
        times = value;
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }
}
